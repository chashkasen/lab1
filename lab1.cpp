#include <iostream>
#include <chrono>
#include <timer.hpp>


void Double(int*& mas, int& n)                                   
{
	int j = 0;      
	int* mas1 = new int[2 * n];  

	for (int i = 0; i < n; i++)                                 
	{

		mas1[j] = mas[i];
		j += 1;
		if (mas[i] %2 ==0)
		{
			mas1[j] = mas[i]; 
			j += 1;
		}
	}

	if (j == 2*n)
		std::cout<<"Худший случай"<<std::endl;
	else if (j == 1.5*n)
		std::cout<<"Средний случай"<<std::endl;
	else if (j == n)
		std::cout<<"Лучший случай"<<std::endl;

	n = j;   
	delete [] mas;      
	mas = new int[n];                  
	for (int i = 0; i < n; i++)
	{
		mas[i] = mas1[i];                  
	}
	delete [] mas1;
}


int main()
{

	setlocale(LC_ALL, "Rus");
	int n;
	
	std::cout << "Введите размер последовательности: " << std::endl;
	std::cin >> n;

	int* mas = new int[n];

	std::cout << "Введите элементы матрицы: " << std::endl;
	for (int i = 0; i < n; i++)
	{
		std::cin >> mas[i];
	}
	Timer t;
	Double(mas, n);
	std::cout << "Time elapsed: " << t.elapsed() << '\n';

	std::cout<<std::endl;
	std::cout<<"Измененный массив: " << std::endl;

	for (int i = 0; i < n; i++)
	{
		std::cout << mas[i] << " ";
	}
		delete [] mas;
		
	return 0;
}

